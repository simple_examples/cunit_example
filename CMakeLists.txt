
################################################################################
# Документация и полезные ссылки относительно CMake:
#   - https://cmake.org/cmake/help/latest/guide/tutorial/index.html
#   - https://cliutils.gitlab.io/modern-cmake/
#   - https://habr.com/ru/post/461817/
#   - https://habr.com/ru/post/330902/
#   - https://habr.com/ru/post/432096/
#   - Дубров, "Система построения проектов CMake", 2015г.
#
################################################################################

###################################################
# Конфигурация проекта
###################################################
cmake_minimum_required(VERSION 3.12)

# Имя проекта
project (
    example_project
    VERSION 0.1
    LANGUAGES C
    DESCRIPTION "CMake project sceleton"
    )

# Директория с выходными бинарниками
set (CMAKE_BINARY_DIR ${PROJECT_SOURCE_DIR}/.bin)

# укажем директории для создания библиотек (static/shared) и исполняемого файла
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

# переменная указывающая на корневую директорию проекта В случае использования
# самостоятельного проекта является синонимом переменной
# CMAKE_CURRENT_SOURCE_DIR, однако есл ироект включен в другой в качестве
# субпроекта, то переменная CMAKE_CURRENT_SOURCE_DIR буфет уже указываьть на топ
# проект, и мы не сможем по ней узнать корневую директорию этого подпроекта
set(SOURCE_DIR_SUBPRJ ${CMAKE_CURRENT_SOURCE_DIR})

if(NOT CMAKE_BUILD_TYPE)
    set (CMAKE_BUILD_TYPE Debug)
endif()
message("  --> build mode: ${CMAKE_BUILD_TYPE}")

# опция включающая детальный вывод сборки make
# Если выключена - make будет собщать только об ворнингах и ошибках
set (CMAKE_VERBOSE_MAKEFILE OFF)
# Опция которую иногда требуют внешние утилиты для работы с проектом,
# например, PVS-studio, YcmComleter и т.д.
set (CMAKE_EXPORT_COMPILE_COMMANDS ON)

# если используется бизи компиляции, и есть в корне проекта директория .vim, то 
# создадим симлинк json DB в эту директорию (нужно для корректной работы
# линтера в Vim)
if(CMAKE_EXPORT_COMPILE_COMMANDS)
    set (COMP_DB_PATH ${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json)
    set (LOCAL_VIM_PRJ_PATH ${PROJECT_SOURCE_DIR}/.vim)
    if(EXISTS ${PROJECT_SOURCE_DIR}/.vim)
        execute_process(
            COMMAND ${CMAKE_COMMAND} -E create_symlink
                    ${COMP_DB_PATH}
                    ${LOCAL_VIM_PRJ_PATH}/compile_commands.json
            )
    endif()
endif()

# Если ON - то все библиотеки проекта будут компилироваться как разделяемые,
# иначе как статические
set (BUILD_SHARED_LIBS OFF)

option(MK_UTEST "build unit tests" ON)
option(MK_DOC   "build doc for project" ON)

# флаги компилятора
# если нужно добавление флагов по условию - просто объявляем 
# еще один блок add_compile_options с нужным флагом внутри условия
add_compile_options(
    
    -std=gnu17
    -pipe
    -Wall
    -Winline
    -Wfloat-equal
    -Wsequence-point
    -Wcast-qual
    -Wshadow
    -Werror-implicit-function-declaration
    -Wno-cast-qual
    )

# флаги линкера
# add_link_options(
#
# )

# подключм файл с cmake модулем
include(cmake_modules/project_aux.cmake)
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
###################################################
# Обработка дочерних CMake файлов
###################################################
unset(TRGT_INCLUDE)
unset(TRGT_LINKS)

set(PATH_TO_PRJ_SRC ${PROJECT_SOURCE_DIR}/src/main)


set(PRJ_LIBNAME "libprj_lib")
list(APPEND TRGT_LINKS ${PRJ_LIBNAME})

add_subdirectory (src/main)

if(MK_UTEST)
    include(CTest)
    enable_testing()
    set(CUNIT_DISABLE_TESTS     ON)
    set(CUNIT_DISABLE_EXAMPLES  ON)
    add_subdirectory (src/tests/cunit)

    add_subdirectory (src/tests/unit_ci)
endif()

if(MK_DOC)
    add_subdirectory (doc)
    execute_process(
        COMMAND ${CMAKE_COMMAND} -E create_symlink
                ${CMAKE_CURRENT_BINARY_DIR}/doc/sphinx/index.html
                ${PROJECT_SOURCE_DIR}/index.html
        )
endif()
