Description
################################################################################
Это пример документации

Пример блока кода на sphinx:

.. code-block:: sh

   rm -rf /

Пример ссылки: `Breath doc`_

Пример документирования функции:

.. doxygenfunction:: sum

Или такой функции:

.. doxygenfunction:: print_string

.. ============================================================================

.. _`Breath doc`:
   https://breathe.readthedocs.io/en/latest/index.html
