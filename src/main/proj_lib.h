#ifndef _TEST_CMAKE_GDB_H_
#define _TEST_CMAKE_GDB_H_

#include <stdio.h>

/**
 * @brief
 * Функция суммирования
 *
 * @param a первый аргумент
 * @param b второй аргумент
 *
 * @return результат суммирования
 */
int sum(int a, int b);

/**
 * Функция вывода на экран соощения "hello world!"
 */
void print_string(void);

#endif /* _TEST_CMAKE_GDB_H_ */
