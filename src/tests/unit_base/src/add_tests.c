/** @file
*********************************************************************
*  @brief Модульные тесты 
*
*********************************************************************
**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "CUnit/CUnit.h"

int suite_lwipc_init(void) ;
int suite_lwipc_clean(void) ;
extern CU_TestInfo tests_lwipc[] ;

static CU_SuiteInfo suites[] =
{
    {   "suite_lwipc    "
      , NULL
      , NULL
      , NULL
      , NULL
      , tests_lwipc
    }
  , CU_SUITE_INFO_NULL
};


void AddTests(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  /* Register suites. */
  if (CUE_SUCCESS != CU_register_suites(suites)) 
  {
    fprintf(stderr, "suite registration failed - %s\n",
      CU_get_error_msg()
    );
    exit(EXIT_FAILURE);
  }
}
