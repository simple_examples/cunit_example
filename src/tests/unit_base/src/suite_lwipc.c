/** @file
*********************************************************************
*  @brief Интеграционные тесты транспорта
*
*********************************************************************
**/
#include <string.h>

#include "CUnit/CUnit.h"





/*=================================================
 * Сами Модульные тесты  
 *=================================================*/
       
static void test_normal(void)
{  
    CU_ASSERT_EQUAL(1, 1);
}

static void test_reply(void)
{
    CU_ASSERT_EQUAL(2, 2);
}
 

CU_TestInfo tests_lwipc[] = 
{
  { "test_normal", test_normal   },
  { "test_reply ", test_reply    },
  CU_TEST_INFO_NULL,
};
