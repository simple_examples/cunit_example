#include <CUnit/CUnitCI.h>
#include "proj_lib.h"


static void test_simple_pass1(void)
{
    CU_ASSERT_FATAL(sum(1,2) == 3);
}

static void test_simple_pass2(void)
{
    CU_ASSERT_FATAL(1 == 1);
}

static int suite_init(void)
{
    return 0;
}

static int suite_clean(void)
{
    return 0;
}

static void test_setup(void)
{
    return;
}

static void test_teardown(void)
{
    return;
}

int main(int argc, char** argv)
{

    CU_CI_add_suite(
          CU_get_basename(argv[0])
        , suite_init
        , suite_clean
        , test_setup
        , test_teardown
    );

    CU_CI_add_test("first test", test_simple_pass1);
    CU_CI_add_test("second test", test_simple_pass2);
    CU_CI_main(argc, argv);
}
