#! /bin/bash

THIS_DIR=$(dirname $(realpath $0))

BUILD_DIR="${THIS_DIR}/.build"
BIN_DIR="${THIS_DIR}/.bin"
VENV_DIR="${THIS_DIR}/.venv"


if [[ $1 == "-r" ]]; then
    if [[ -d ${BUILD_DIR} ]]; then
        rm -rf ${BUILD_DIR}/*
    fi

    if [[ -d ${BIN_DIR} ]]; then
        rm -rf ${BIN_DIR}
    fi

    if [[ -d ${VENV_DIR} ]]; then
        rm -rf ${VENV_DIR}
    fi
fi

# if .venv does not exist, create it
if [[ ! -d $VENV_DIR ]]; then
    python3 -m venv .venv
    if [[ $? != 0 ]]; then
        echo "ERROR: Unable to create .venv dir. Exiting"
        exit 1
    fi
fi

# activate venv
source $VENV_DIR/bin/activate

# If there is a file with the required packages - install them
REQ_FILE=$THIS_DIR/requirements.txt
if [[ -f $REQ_FILE ]]; then
    python3 -m pip install -r $REQ_FILE -q -q
    if [[ $? != 0 ]]; then
        echo "ERROR: Failed to install dependencies. Exiting"
        exit 1
    fi
fi

cmake -B ${BUILD_DIR}
if [[ $? != 0 ]]; then
    echo "ERROR: Fail on cmake"
    exit 1
fi

cmake --build ${BUILD_DIR} --parallel 8
exit $?
